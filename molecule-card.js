(function() {

  'use strict';

  Polymer({

    is: 'molecule-card',

    behaviors: [
      Polymer.i18nBehavior
    ]

  });

}());

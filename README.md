# molecule-card

Your component description.

Example:
```html
<molecule-card></molecule-card>
```

```

## Styling

The following custom properties and mixins are available for styling:

| Custom property | Description     | Default        |
|:----------------|:----------------|:--------------:|
| --molecule-card-scope      | scope description | default value  |
| --molecule-card  | empty mixin     | {}             |
